API PATH: /wp-json/<entity>
=====

Students
=====

Create a Student
-------------

	POST /students

  Requires [authentication](http://wp-api.org/guides/authentication.html)
  ***Use Basic Authentication for now***

### Input
The `data` parameter consists of the elements of the Post object to be
created.  This data can be submitted via a regular HTTP multipart body, with
the Post keys and values set to the `data` parameter, or through a direct JSON
body.

That is, the following are equivalent:

Content-Type: application/x-www-form-urlencoded

  data[title]=Hello%20World!


Content-Type: application/json

  {"title":"Hello World!"}

The `data` parameter should be an object containing the following key value
pairs:

* `title` - {student_first_name} {student_last_name} __*required*__
* `name` - Slug of the post. (string) *optional*
* `status` - `publish`
* `type` - ams_student
* `fields` - object containing the following fields:
    * `student_first_name` - `String` __*required*__
    * `student_last_name` - `String` __*required*__
    * `student_country` - `String` __*required*__
    * `student_email` - `String` __*required*__
    * `student_time_zone` - `String`, moment.js compatible timezone string, i.e. "CET"
    * `student_preferred_contact_number` - `String`, Available options: 'Mobile', 'Home', 'Work', 'Alternate', 'Skype'
    * `student_mobile_phone` - `String`
    * `student_work_phone` - `String`
    * `student_home_phone` - `String`
    * `student_alternate_phone` - `String`
    * `student_title` - `String`, Available options: "Mr.", "Mrs.", "Miss."
    * `student_occupation` - `String`
    * `student_age_group` - `String`, Available options: "25-34", "35-44", "45-60", "61+"
    * `student_skypeid` - `String`
    * `student_city` - `String`
    * `student_state` - `String`
    * `student_zip` - `String`
    * `student_siebel_ref` - `String`
    * `office` - `String`, Available options: "USA", "CAN", "GBR", "INT", "ZAF"


### Response
On a successful creation, a 201 Created status is given, indicating that the
student has been created.

The new Student entity is also returned in the body for convienience.

If the client is not authenticated, a 403 Forbidden response is given.

Retrieve Students
--------------
The Students endpoint returns a Student Collection containing a subset of the existing students.

	GET /students


Retrieve a Student
---------------

	GET /students/<id>

### Input

    The `filter` parameter can be used to filter the query using any field of the entity.
    Example: students?filter[meta_query][student_first_name]=John
    This will return all students that have their first name equal to `John`


### Response
The response is a Student entity containing the requested Student if available.


Edit a Student
-----------

	PUT /students/<id>

Requires [authentication](http://wp-api.org/guides/authentication.html)
***Use Basic Authentication for now***

For compatibility reasons, this endpoint also accepts the POST and PATCH
methods. Both of these methods have the same behaviour as using PUT. It is
recommended to use PUT if available to fit with REST convention.

### Input
The `data` parameter consists of Post ID and the elements of the Post object
to be modified.  This data can be submitted via a regular HTTP multipart body,
with the Post keys and values set to the `data` parameter, or through a direct
JSON body.  See the Create Post endpoint for an example.

The `data` parameter should be an object containing the following key value
pairs:

* `ID` - Unique ID of the student. (integer) __*required*__
* `title` - {student_first_name} {student_last_name} __*required*__
* `name` - Slug of the post. (string) *optional*
* `status` -  `String` : Use "publish"
* `type` - ams_student
* `fields` - object containing the following fields:
    * `student_first_name` - `String` __*required*__
    * `student_last_name` - `String` __*required*__
    * `student_email` - `String` __*required*__
    * `student_country` - `String` __*required*__
    * `student_time_zone` - `String`, PHP compatible timezone string, i.e. "CET", refer to http://uk.php.net/manual/en/timezones.php
    * `student_preferred_contact_number` - `String`, Available options: 'Mobile', 'Home', 'Work', 'Alternate', 'Skype'
    * `student_mobile_phone` - `String`
    * `student_work_phone` - `String`
    * `student_home_phone` - `String`
    * `student_alternate_phone` - `String`
    * `student_title` - `String`, Available options: "Mr.", "Mrs.", "Miss."
    * `student_occupation` - `String`
    * `student_age_group` - `String`, Available options: "25-34", "35-44", "45-60", "61+"
    * `student_skypeid` - `String`
    * `student_city` - `String`
    * `student_state` - `String`
    * `student_zip` - `String`
    * `student_siebel_ref` - `String`
    * `office` - `String`, Available options: "USA", "CAN", "GBR", "INT", "ZAF"


### Response
On a successful update, a 200 OK status is given, indicating the student has been
updated. The updated Student entity is returned in the body.

If the client is not authenticated, a 403 Forbidden response is sent.

Events
=====

Create a Event
-------------

  POST /events

  Requires [authentication](http://wp-api.org/guides/authentication.html)
  ***Use Basic Authentication for now***

### Input
The `data` parameter consists of the elements of the Event object to be
created.  This data can be submitted via a regular HTTP multipart body, with
the Post keys and values set to the `data` parameter, or through a direct JSON
body.

That is, the following are equivalent:

Content-Type: application/x-www-form-urlencoded

  data[title]=Hello%20World!


Content-Type: application/json

  {"title":"Hello World!"}

The `data` parameter should be an object containing the following key value
pairs:

* `title` - Event title __*required*__
* `status` - `publish`
* `type` - ams_event
* `fields` - object containing the following fields:
    * `event_code` - `String` : e.g. "CWWRES1114YYCA"
    * `event_week` - `String` : e.g. "32"
    * `event_type` - `String` : Available options: "Preview", "Basic Training", "Core Elite", "Advanced"
    * `event_start_date` - `String` : e.g. "31/12/2014" (format dd/mm/yyyy)
    * `event_start_time` - `String` : e.g. "11:42" (format military)
    * `event_market` - `String`
    * `event_field_manager` - `String`
    * `event_brand` - `String`|`Int` : Brand name or API relevant WP Post ID, e.g. "Brick Buy Brick" or 14
    * `event_country` - `String`
    * `event_color` - `String` : Hexadecimal color code, e.g. #FFF
    * `event_students` - `Array` : containing IDs of existing `Students`
    * `event_field_manager_email` - `String`
    * `event_status` - `String` : Available options: "New", "In Progress", "Closed"
    * `event_speaker` - `String`
    * `event_hsk` - `String`


### Response
On a successful creation, a 201 Created status is given, indicating that the
event has been created.

The new Event entity is also returned in the body for convienience.

If the client is not authenticated, a 403 Forbidden response is given.

Retrieve Events
--------------
The Events endpoint returns an Event Collection containing a subset of the existing events.

  GET /events


Retrieve an Event
---------------

  GET /events/<id>

### Input

    The `filter` parameter can be used to filter the query using any field of the entity.
    Example: /events?filter[meta_query][event_type]=Basic Training
    This will return all events of type `Basic Training`

### Response
The response is a Event entity containing the requested Event if available.


Edit an Event
-----------

  PUT /events/<id>

Requires [authentication](http://wp-api.org/guides/authentication.html)
***Use Basic Authentication for now***

For compatibility reasons, this endpoint also accepts the POST and PATCH
methods. Both of these methods have the same behaviour as using PUT. It is
recommended to use PUT if available to fit with REST convention.

### Input
The `data` parameter consists of Event ID and the elements of the Event object
to be modified.  This data can be submitted via a regular HTTP multipart body,
with the Event keys and values set to the `data` parameter, or through a direct
JSON body.  See the Create Student  endpoint for an example.

The `data` parameter should be an object containing the following key value
pairs:

* `title` - Event title __*required*__
* `status` - `publish`
* `type` - ams_event
* `fields` - object containing the following fields:
    * `event_code` - String, e.g. CWWRES1114YYCA
    * `event_week` - String
    * `event_type` - Choose from: Preview, Basic Training, Core Elite, Advanced
    * `event_start_date` - String
    * `event_start_time` - String
    * `event_market` - String
    * `event_field_manager` - String
    * `event_brand` - String
    * `event_country` - String
    * `event_color` - Hexadecimal color code, i.e. #FFF
    * `event_students` - Array containing ids of existing students
    * `event_field_manager_email` - String
    * `event_status` - Choose from: New, In Progress, Closed
    * `event_speaker` - String
    * `event_hsk` - String


### Response
On a successful update, a 200 OK status is given, indicating the student has been
updated. The updated Event entity is returned in the body.

If the client is not authenticated, a 403 Forbidden response is sent.

Orders
=====

Create an Order
-------------

  POST /orders

  Requires [authentication](http://wp-api.org/guides/authentication.html)
  ***Use Basic Authentication for now***

### Input
The `data` parameter consists of the elements of the Order object to be
created.  This data can be submitted via a regular HTTP multipart body, with
the Post keys and values set to the `data` parameter, or through a direct JSON
body.

That is, the following are equivalent:

Content-Type: application/x-www-form-urlencoded

  data[title]=Hello%20World!


Content-Type: application/json

  {"title":"Hello World!"}

The `data` parameter should be an object containing the following key value
pairs:

* `title` - Order no. {order_number} __*required*__
* `status` - `publish`
* `type` - ams_order
* `fields` - object containing the following fields:
    * `order_student_id` - `Int` __*required*__ Student entity ID (Wordpress Post ID)
    * `order_event` - `Int`|`String` __*required*__ Event entity ID (Wordpress Post ID) or Event code
    * `order_cycle` - `String`
    * `order_status` - `String`, Available options: "Live", "Booked", "Cancelled", "Complete"
    * `order_brand` - `String`
    * `order_number` - `String`
    * `order_line_item` - `String`
    * `order_basic_training_start_date` - `String`
    * `order_preview_event_code` - `String`
    * `order_package` - `String`
    * `order_pairing_type` - `String`, Available options: "Single", "Contract Couple"
    * `order_primary_or_guest` - `String`, Available options: "Primary", "Guest"
    * `order_primary_name` - `String`
    * `order_updownfrom` - `String`
    * `order_new_pairing_qty` - `String`
    * `order_event_type` - `String`, Available options: "Preview", "Basic Training", "Core Elite", "Advanced"
    * `order_core_event_code` - "String"
    * `order_sales_source` - "String"


### Response
On a successful creation, a 201 Created status is given, indicating that the
event has been created.

The new Order entity is also returned in the body for convienience.

If the client is not authenticated, a 403 Forbidden response is given.

Retrieve Order
--------------
The Order endpoint returns an Order Collection containing a subset of the existing orders.

  GET /orders


Retrieve an Order
---------------

  GET /orders/<id>


### Response
The response is a Order entity containing the requested Order if available.


Edit an Order
-----------

  PUT /orders/<id>

Requires [authentication](http://wp-api.org/guides/authentication.html)
***Use Basic Authentication for now***

For compatibility reasons, this endpoint also accepts the POST and PATCH
methods. Both of these methods have the same behaviour as using PUT. It is
recommended to use PUT if available to fit with REST convention.

### Input
The `data` parameter consists of Event ID and the elements of the Order object
to be modified.  This data can be submitted via a regular HTTP multipart body,
with the Event keys and values set to the `data` parameter, or through a direct
JSON body.  See the Create Order  endpoint for an example.

The `data` parameter should be an object containing the following key value
pairs:

* `title` - Order no. {order_number} __*required*__
* `status` - `publish`
* `type` - ams_order
* `fields` - object containing the following fields:
    * `order_cycle` - String
    * `order_status` - Choose from: Live, Booked, Cancelled, Complete
    * `order_student_id` - String
    * `order_brand` - String
    * `order_number` - String
    * `order_line_item` - String
    * `order_event` - String
    * `order_basic_training_start_date` - String
    * `order_preview_event_code` - String
    * `order_package` - String
    * `order_pairing_type` - Single, Contract Couple
    * `order_primary_or_guest` - Choose from: Primary, Guest
    * `order_primary_name` - String
    * `order_updownfrom` - String
    * `order_new_pairing_qty` - String
    * `order_event_type` - Choose from: Preview, Basic Training, Core Elite, Advanced
    * `order_core_event_code` - String
    * `order_sales_source` - String


### Response
On a successful update, a 200 OK status is given, indicating the order has been
updated. The updated Order entity is returned in the body.

If the client is not authenticated, a 403 Forbidden response is sent.

Appendix A: JSON Schema
=======================
The JSON Schema describing the entities in this document is available in
schema.json.
